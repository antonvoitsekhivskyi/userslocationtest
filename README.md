# README #

## UsersLocationTest app ##

Application shows current user location and locations for every registered user.

### Key points: ###

1. For backend used BaaS - solution "Firebase".
2. Shows map with current user location. Other users are marked with PointAnnotations on the map.
3. When select the user from Users List, map shows location for selected user and his/her name (email). 
4. Users List and Map is showing in landscape for iPhone simultaneously.
5. As a third-party dependencies manager CocoaPods is using.

Perform all work by open UsersLocationTest.xcworkspace file in Xcode. 