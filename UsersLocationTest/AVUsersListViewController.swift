//
//  AVUsersListViewController.swift
//  UsersLocationTest
//
//  Created by Family Mac on 2/12/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit

class AVUsersListViewController: UITableViewController, AVUsersDataProviderDelegate
{
	var mapViewController: AVMapViewController? = nil
	var users = [AVUser]()
	
	override func viewDidLoad()
	{
		super.viewDidLoad()

		if let split = self.splitViewController
		{
		    let controllers = split.viewControllers
		    self.mapViewController = (controllers[controllers.count - 1] as!
						 UINavigationController).topViewController as? AVMapViewController
		}
		AVUsersDataProvider.sharedDataProvider.add(delegate: self)
	}

	override func viewWillAppear(_ animated: Bool)
	{
		self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
		super.viewWillAppear(animated)
	}

	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?)
	{
		if segue.identifier == "showDetail"
		{
			let controller = (segue.destination as! UINavigationController).topViewController as!
						AVMapViewController
			controller.navigationItem.leftBarButtonItem =
						self.splitViewController?.displayModeButtonItem
			controller.navigationItem.leftItemsSupplementBackButton = true
			self.mapViewController = controller
			controller.select(user: self.users[self.tableView.indexPathForSelectedRow!.row])
		}
	}
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
	{
		var result = true
		guard let spit = self.splitViewController else { return result }
		if identifier == "showDetail", !spit.isCollapsed
		{
			guard let mapVC = self.mapViewController else { return result }
			result = false
			mapVC.select(user: self.users[self.tableView.indexPathForSelectedRow!.row])
			
		}
		return result
	}

	// MARK: - Table View

	override func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return self.users.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
				UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		let user = self.users[indexPath.row]
		cell.textLabel!.text = user.name
		return cell
	}
	
	//MARK: - AVUsersDataProviderDelegate implementation
	
	func changed(users: [AVUser])
	{
		self.users = users
		self.tableView.reloadData()
	}
}
