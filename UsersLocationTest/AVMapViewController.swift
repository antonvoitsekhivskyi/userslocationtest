//
//  AVMapViewController.swift
//  UsersLocationTest
//
//  Created by Family Mac on 2/12/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import UIKit
import MapKit

class AVMapViewController: UIViewController, MKMapViewDelegate, AVUsersDataProviderDelegate,
			CLLocationManagerDelegate
{
	@IBOutlet weak var map: MKMapView!
	
	private let locationManager = CLLocationManager()
	private var userToSelect: AVUser?
	
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		AVUsersDataProvider.sharedDataProvider.add(delegate: self)

		if CLLocationManager.authorizationStatus() == .notDetermined
		{
			self.locationManager.delegate = self
			self.locationManager.requestAlwaysAuthorization()
		}
		else
		{
			self.map.showsUserLocation = true
			self.locationManager.startUpdatingLocation()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		self.map.delegate = nil
		self.locationManager.delegate = nil
		AVUsersDataProvider.sharedDataProvider.remove(delegate: self)
		super.viewWillDisappear(animated)
	}
	
	func select(user: AVUser)
	{
		self.userToSelect = nil
		// map is nod loaded yet - postpone user selection
		guard let mapView = self.map else
		{
			self.userToSelect = user
			return
		}
	
		var coordinatesToZoom: CLLocationCoordinate2D? = nil
		if user != AVUsersDataProvider.sharedDataProvider.currentUser
		{
			//If selected user is not currently authenticated user, should select the approperiate 
			//annotation
			var needToSelectAfterUpdate = true
			for annotation in self.map.annotations
			{
				if nil != annotation.title && user.name == annotation.title!
				{
					mapView.selectAnnotation(annotation, animated: false)
					coordinatesToZoom = annotation.coordinate
					needToSelectAfterUpdate = false
					break
				}
			}
			if needToSelectAfterUpdate
			{
				//no annotation is present for selected user - postpone user selection
				self.userToSelect = user
			}
		}
		else
		{
			coordinatesToZoom = user.coordinate
		}
		
		// zoom to selected user region
		if let validCoordinates = coordinatesToZoom,
					validCoordinates.latitude != kCLLocationCoordinate2DInvalid.latitude,
					validCoordinates.longitude != kCLLocationCoordinate2DInvalid.longitude
		{
			let region = MKCoordinateRegionMakeWithDistance(validCoordinates, 800, 800)
			mapView.setRegion(self.map.regionThatFits(region), animated: true)
		}
		else
		{
			//coordinates are not valid - postpone user selection
			self.userToSelect = user
		}
	}
	
	private func removeNotExistedAnnotationsAndUpdateExisted(for users: [AVUser])
	{
		for annotation in self.map.annotations
		{
			guard let pointAnnotation = annotation as? MKPointAnnotation else { continue }
			guard let title = pointAnnotation.title else { continue }
			var annotationExistsInUsers = false
			for user in users
			{
				if title == user.name
				{
					pointAnnotation.coordinate = user.coordinate
					annotationExistsInUsers = true
				}
			}
			if !annotationExistsInUsers
			{
				self.map.removeAnnotation(pointAnnotation)
			}
		}
	}
	
	private func addNewAnnotations(for users: [AVUser])
	{
		for user in users
		{
			if user.uuid == AVUsersDataProvider.sharedDataProvider.currentUser?.uuid
			{
				// do not create annotation for current user - a default blue dot marker is
				// already presented on a map
				continue
			}
			var userExistsOnMap = false
			for annotation in self.map.annotations
			{
				guard let pointAnnotation = annotation as? MKPointAnnotation else { continue }
				guard let title = pointAnnotation.title else { continue }
				if title == user.name
				{
					userExistsOnMap = true
					break
				}
			}
			if !userExistsOnMap
			{
				let annotation = MKPointAnnotation()
				annotation.coordinate = user.coordinate
				annotation.title = user.name
				self.map.addAnnotation(annotation)
			}
		}
	}

	//MARK: - AVUsersDataProviderDelegate implementation
	
	func changed(users: [AVUser])
	{
		// When changes occures, need to update map:
		// 1. Remove annotations for users, that are removed from Firebase server
		// 2. Update coordinates for other annotations
		// 3. Add new annotations for newly created users on Firebase server
		self.removeNotExistedAnnotationsAndUpdateExisted(for: users)
		self.addNewAnnotations(for: users)
		
		// perform user posponed selection
		if let selectedUser = self.userToSelect
		{
			self.select(user: selectedUser)
		}
	}
	
	//MARK: - MKMapViewDelegate implementation
	
	func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation)
	{
		AVUsersDataProvider.sharedDataProvider.currentUser?.coordinate = userLocation.coordinate
	}
	
	//MARK: - CLLocationManagerDelegate implementation
	
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status:
				CLAuthorizationStatus)
	{
		if status == .authorizedAlways
		{
			manager.startUpdatingLocation()
		}
	}
}

