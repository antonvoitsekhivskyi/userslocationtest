//
//  AVLoginViewController.swift
//  UsersLocationTest
//
//  Created by Family Mac on 3/12/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import Foundation
import UIKit
import Firebase

enum AVAuthErrorType
{
    case signIn
	 case signUp
}

class AVLoginViewController: UIViewController
{
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var controllsContainer: UIView!
	
	var loginPerformed = false
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
	
		self.controllsContainer.alpha = 0.0
		FIRAuth.auth()!.addStateDidChangeListener()
		{ [unowned self] auth, user in
			if !self.loginPerformed
			{
				if let succededUser = user
				{
					self.loginSuccededFor(user: succededUser)
				}
				else
				{
					self.showControllsContainer()
				}
			}
		}
	}
	
	@IBAction func loginAction()
	{
		FIRAuth.auth()!.signIn(withEmail: emailTextField.text!,
					password: passwordTextField.text!)
		{ [unowned self] user, error in
			if let errorToShow = error
			{
				self.showErrorAlert(errorToShow, type: .signIn)
			}
		}
	}
	
	@IBAction func signUpAction()
	{
		FIRAuth.auth()!.createUser(withEmail: emailTextField.text!,
					password: passwordTextField.text!)
		{ [unowned self] user, error in
			if error == nil && user != nil
			{
				AVUsersDataProvider.sharedDataProvider.register(user: user!)
				FIRAuth.auth()!.signIn(withEmail: self.emailTextField.text!,
							password: self.passwordTextField.text!)
			}
			else if let errorToShow = error
			{
				self.showErrorAlert(errorToShow, type: .signUp)
			}
		}
	}
	
	private func showErrorAlert(_ error: Error, type: AVAuthErrorType)
	{
		let title = (type == .signIn) ? "Unable to Sign In" : "Unable to Sign Up"
		let theAlert = UIAlertController(title: title, message:
					error.localizedDescription, preferredStyle: .alert)
		theAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		self.present(theAlert, animated: true, completion: nil)
	}
	
	private func showControllsContainer()
	{
		UIView.animate(withDuration: 0.5, animations:
		({
			self.controllsContainer.alpha = 1.0
		}))
	}
	
	private func loginSuccededFor(user: FIRUserInfo)
	{
		self.loginPerformed = true
		AVUsersDataProvider.sharedDataProvider.authorize(user: user)
		self.performSegue(withIdentifier: "AVShowSplitViewSegueIdentifier", sender: nil)
	}
	
}
