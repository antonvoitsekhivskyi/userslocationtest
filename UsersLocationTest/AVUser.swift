//
//  AVUser.swift
//  UsersLocationTest
//
//  Created by Family Mac on 3/12/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class AVUser: Equatable
{
	let uuid: String
	let name: String
	var coordinate: CLLocationCoordinate2D
	{
		didSet
		{
			let currentCoordUserEndpoint = AVUsersDataProvider.usersEndpoint.child(self.uuid)
						.child("coord")
			currentCoordUserEndpoint.setValue(["latitude":coordinate.latitude.description,
						"longitude":coordinate.longitude.description])
		}
	}

// step by step, parse the expected json:
//
// {
//		user_id
//		{
//			name = someName@mail
//			coord
//			{
//				latitude = 55.0000
//				longitude = 66.0000
//			}
//		}
//	}

	convenience init?(snapshotValue: [String: AnyObject], uuid: String)
	{
		guard let theName = snapshotValue["name"] as? String else { return nil }
		guard let coordValue = snapshotValue["coord"] as? [String: AnyObject] else { return nil }
		guard let latitudeValue = coordValue["latitude"] as? String else { return nil }
		guard let longitudeValue = coordValue["longitude"] as? String else { return nil }
		guard	let latitude = Double(latitudeValue) else { return nil }
		guard	let longitude = Double(longitudeValue) else { return nil }
		self.init(uuid: uuid, name:theName, coordinate:CLLocationCoordinate2D(latitude: latitude,
					longitude: longitude))
	}

	convenience init(userInfo: FIRUserInfo)
	{
		let email = userInfo.email ?? ""
		self.init(uuid: userInfo.uid, name:email, coordinate:kCLLocationCoordinate2DInvalid)
	}
	
	init(uuid: String, name: String, coordinate: CLLocationCoordinate2D)
	{
		self.uuid = uuid
		self.name = name
		self.coordinate = coordinate
	}
	
	static func ==(lhs: AVUser, rhs: AVUser) -> Bool
	{
		return lhs.uuid == rhs.uuid
	}
}
