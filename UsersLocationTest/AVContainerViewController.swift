//
//  AVContainerViewController.swift
//  UsersLocationTest
//
//  Created by Family Mac on 2/12/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import Foundation
import UIKit

//! Class, that is respond to manage the split view behave.
//! According to WWDC2014 216 Session, to change the split view behavior, it should be embedded
//! into a Container, that would manage its trait collections via <code>willTransition:to
//! newCollection: with coordinator:</code> method
class AVContainerViewController : UIViewController, UISplitViewControllerDelegate
{
	var splitView: UISplitViewController!

	override func addChildViewController(_ childController: UIViewController)
	{
		if let splitViewCandidate = childController as? UISplitViewController
		{
			self.splitView = splitViewCandidate
		}
		super.addChildViewController(childController)
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		// if App is launched in landscape - call override when child is added
		self.overrideChildsTraitCollectionIfNeeded(self.traitCollection)
		super.viewWillAppear(animated)
	}

	override func willTransition(to newCollection: UITraitCollection, with coordinator:
				UIViewControllerTransitionCoordinator)
	{
		self.overrideChildsTraitCollectionIfNeeded(newCollection)
	}
	
	private func overrideChildsTraitCollectionIfNeeded(_ traitCollection: UITraitCollection)
	{
		if traitCollection.verticalSizeClass == .compact
		{
			setOverrideTraitCollection(UITraitCollection(horizontalSizeClass: .regular),
						forChildViewController: self.splitView)
		}
		else if traitCollection.horizontalSizeClass == .compact
		{
			setOverrideTraitCollection(UITraitCollection(horizontalSizeClass: .compact),
						forChildViewController: self.splitView)
		}
	}
}
