//
//  AVUsersDataProvider.swift
//  UsersLocationTest
//
//  Created by Family Mac on 3/12/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

import Foundation
import Firebase
import MapKit

/**
	Protocol for AVUsersDataProvider delegate
*/
protocol AVUsersDataProviderDelegate : class
{
	/**
		Called, when object registers as a listener and when futher updates of users occures
	*/
	func changed(users: [AVUser])
}

class AVUsersDataProvider
{
	/**
		Singletone instance of AVUsersDataProvider
	*/
	static let sharedDataProvider = AVUsersDataProvider()
	
	/**
		Root endpoint for all users on Firebase server
	*/
 	static let usersEndpoint = FIRDatabase.database().reference(withPath: "users-list")

	/**
		AVUser instance of authorized user. Becomes initialized after authorize
	*/
	private(set) var currentUser: AVUser?
	
	private var users = [AVUser]()
	private var delegates = [AVUsersDataProviderDelegate]()
	
	/**
		Creates endpoint for given user on Firebase server
	*/
	func register(user: FIRUserInfo)
	{
		let currentCoordUserEndpoint = AVUsersDataProvider.usersEndpoint.child(user.uid)
		currentCoordUserEndpoint.setValue(["name":user.email!])
	}

	/**
		Creates currentUser and start listen for updates on Firebase server
	*/
	func authorize(user: FIRUserInfo)
	{
		self.currentUser = AVUser(userInfo: user)

		AVUsersDataProvider.usersEndpoint.observe(.value, with:
		{ snapshot in
			if snapshot.exists(), let snapshotValue = snapshot.value as? [String: AnyObject]
			{
				var newUsers = [AVUser]()
				for (uuid, value) in snapshotValue
				{
					if let valueToResolve = value as? [String: AnyObject]
					{
						if let user = AVUser(snapshotValue: valueToResolve, uuid:uuid)
						{
							newUsers.append(user)
						}
					}
				}
				self.users = newUsers
				self.delegates.forEach { $0.changed(users: self.users) }
			}
		})
	}

	/**
		Adds new delegate to listen for the users changes
		Calls the delegate callback immidietly after adding with current users and when users changes
	*/
	func add(delegate: AVUsersDataProviderDelegate)
	{
		self.delegates.append(delegate)
		if !self.users.isEmpty
		{
			delegate.changed(users: self.users)
		}
	}

	/**
		Removes passed delegate as a listerner
	*/
	func remove(delegate: AVUsersDataProviderDelegate)
	{
		self.delegates = self.delegates.filter() { return $0 !== delegate }
	}
}
